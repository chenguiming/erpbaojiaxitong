import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from  '@/views/Login.vue'
import Home from '@/views/Home.vue'
import Index from '@/views/Index.vue'

import UserList from '@/views/user/UserList.vue'
import RoleList from '@/views/role/RoleList.vue'
import Customer from '@/views/information/Customer.vue'
import Addcustomer from '@/views/information/add/Addcustomer.vue'
import AddLinkman from '@/views/information/add/AddLinkman.vue'
import LinkMan from '@/views/information/LinkMan.vue'
import Menus from '@/views/menu/Menus.vue'
import Process from "../views/information/Process.vue"
import Material from '@/views/information/Material.vue'
import Addaddmaterial from '@/views/information/add/Addaddmaterial.vue'
import AddProcess from "../views/information/add/AddProcess.vue"
import OrderList from "../views/order/OrderList.vue"
import AddOrder from "../views/order/add/AddOrder.vue"
import CraftList from "../views/craft/CraftList.vue"
import ckCraftList from "../views/craft/add/ckCraftList.vue"
import Client from '@/views/customer/Client.vue'
import CustomerLogin from '@/views/customer/CustomerLogin'

import OrderCus from '@/views/customer/OrderCus'
import Addpay from '@/views/customer/Addpay'
import Paylist from '@/views/customer/PayList'
import PayResult from '@/views/customer/PayResult'
import InvoiceList from '@/views/invoice/InvoiceList.vue'
Vue.use(VueRouter)

const routes = [
  {path:'/',redirect:'/login'},
  {name:'登录',path:'/login',component:Login},
  {name:'用户登录', path: '/customerLogin', component: CustomerLogin},
  {name:'主页',path:'/home',component:Home,children:[
      {name:'首页',path:'/index',component:Index},
      {name:'用户管理',path:'/userList',component:UserList},
      {name:'角色管理',path:'/roleList',component:RoleList},
      {name:'菜单管理',path:'/menus',component:Menus},
      {name:'客户管理',path:'/customer',component:Customer},
      {name:'添加',path:'/addcustomer',component:Addcustomer},
      {name:'原材料管理',path:'/material',component:Material},
      {name:'原材料添加',path:'/addaddmaterial',component:Addaddmaterial},
      {name:'工艺',path:'/process',component:Process},
      {name:'客户管理',path:'/customer',component:Customer},
      {name:'联系人表',path:'/linkMan',component:LinkMan},
      {name:'联系人管理',path:'/addLinkman',component:AddLinkman},
      {name:'添加',path:'/addcustomer',component:Addcustomer},
      {name:'原材料管理',path:'/material',component:Material},
      {name:'添加工艺',path:'/addProcess',component:AddProcess},
      {name:'订单列表',path:'/orderList',component:OrderList},
      {name:'添加订单',path:'/addOrder',component:AddOrder},
      {name:'工艺列表',path:'/craftList',component:CraftList},
      {name:'BOM明细',path:'/ckCraftList',component:ckCraftList},
      {name:'发票列表',path:'/invoiceList',component:InvoiceList},
    ],
  },
  {name:'客户端',path:'/client',component:Client,children: [
      {name:'订单查看',path:'/orderCus',component:OrderCus},
      {name:'发票申请',path:'/addpay',component:Addpay},
      {name:'发票列表',path:'/paylist',component:Paylist},
      {name:'支付结果',path:'/payResult',component:PayResult},
    ]},
]

const router = new VueRouter({
  routes
})

router.beforeEach(function (to, from, next) {//to 路由目标 //from  //next 拦截补拦截，拦截到哪 路由守卫


  if(to.path=='/login' || to.path=='/customerLogin'){ //当访问登录组件不拦截

    return next();
  }

  let user=sessionStorage.getItem("user");//获取sessionStorage中用户的信息
  let customer=sessionStorage.getItem("customer");//获取sessionStorage中用户的信息

  if(user){
    //当前用户已登录
    return next();//继续访问
  }
  if(customer){
    //当前用户已登录
    return next();//继续访问
  }

  return  next('/login');
})


export default router
