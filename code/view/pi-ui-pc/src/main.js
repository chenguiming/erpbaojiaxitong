import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/css/global.css'
import axios from 'axios'
import Moment from 'moment'
import echarts from 'echarts'

Vue.config.productionTip = false

Vue.use(ElementUI);

axios.defaults.baseURL="/api"; //前缀

//axios提供拦截器，发送或响应之前或者之后都能拦截
axios.interceptors.request.use((req)=>{
  //获取token
 let token = sessionStorage.getItem("token");
 if(token!=null){
   req.headers.token=token;
 }
  return req;
});


// 挂载 axios（相当于声明了一个vue的全局变量，命名，this.$http就可以使用了）
Vue.prototype.$http = axios

Vue.prototype.$echarts = echarts

//自定义moment全局过滤器
Vue.filter('datefmt',function(data){
  return Moment(data).format("YYYY-MM-DD HH:mm:ss");
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
