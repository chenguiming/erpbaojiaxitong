module.exports={
  devServer:{
    port:8888,
    proxy:{
      '/api':{
        target:'http://localhost:8080',
        changeOrigin:'true',
        pathRewrite: {
          '^/api':''
        }
      }
    }
  },
  lintOnSave: false  //关闭代码检查 eslint的插件， 1,默认代码换行最多一行.2,代码结尾统一不能加分号 3,使用单引号
}



