package com.bw.controller.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.bw.entity.user.TbRole;
import com.bw.resp.ResponseResult;
import com.bw.service.user.ITbRoleService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@RestController
@RequestMapping("role")
public class TbRoleController {

    @Autowired
    private ITbRoleService roleService;

    /**
     * 角色列表信息接口
     */
    @PostMapping("list/{page}/{size}")
    public ResponseResult list(@PathVariable("page") int page, @PathVariable("size") int size, @RequestBody TbRole tbRole) { //参数json字符串
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper queryWrapper = new QueryWrapper();
            if (null != tbRole.getRoleName()) {
                queryWrapper.like("ROLE_NAME", "%" + tbRole.getRoleName() + "%");
            }
            PageHelper.startPage(page, size);//开启分页查询
            List<TbRole> roles = roleService.list(queryWrapper);//列表查询
            PageInfo pageInfo = new PageInfo(roles);//pagehelper封装好的分页实体类，将数据放进去
            result.setResult(pageInfo);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败:" + e.toString());
        }
        return result;
    }
    /**
     * 角色信息保存接口
     */
    @PostMapping("save")
    public ResponseResult save(@RequestBody TbRole tbRole) { //参数json字符串
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            //判断角色名是否存在
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("ROLE_NAME", tbRole.getRoleName());
            List<TbRole> roles = roleService.list(queryWrapper);
            if (roles.size() > 0 && null == tbRole.getId()) {
                result.setSuccess(false);
                result.setMessage("保存失败:角色已存在");
                return result;
            }
            roleService.saveOrUpdate(tbRole);
            result.setMessage("保存成功");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("保存失败:" + e.toString());
        }
        return result;
    }


    /**
     * 角色信息查看接口
     */
    @GetMapping("get/{id}")
    public ResponseResult get(@PathVariable("id") Long id) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            TbRole tbRole=roleService.getById(id);
            result.setResult(tbRole);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败:" + e.toString());
        }
        return result;
    }

    /**
     * 查询全部角色
     */
    @GetMapping("getRoles")
    public ResponseResult getRoles() {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            List<TbRole> tbRoles = roleService.list();
            result.setResult(tbRoles);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败:" + e.toString());
        }
        return result;
    }
}
