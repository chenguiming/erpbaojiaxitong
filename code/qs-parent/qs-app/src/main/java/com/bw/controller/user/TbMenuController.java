package com.bw.controller.user;



import com.bw.entity.user.TbMenu;
import com.bw.resp.ResponseResult;
import com.bw.resp.TreeMenuVo;
import com.bw.service.user.ITbMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@RestController
@RequestMapping("menu")
public class TbMenuController {

    @Autowired
    private ITbMenuService tbMenuService;


    @PostMapping("save")
    public ResponseResult save(@RequestBody TbMenu menu) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            tbMenuService.saveOrUpdate(menu);
            result.setMessage("保存成功");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("保存失败:" + e.toString());
        }
        return result;
    }

    @GetMapping("getTree")
    public ResponseResult getTree() {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            List<TreeMenuVo> list = tbMenuService.getTree();
            result.setResult(list);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败:" + e.toString());
        }
        return result;
    }
    @GetMapping("get/{id}")
    public ResponseResult get(@PathVariable("id") Long id) {//回显菜单
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            TbMenu menu = tbMenuService.getById(id);
            result.setResult(menu);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败:" + e.toString());
        }
        return result;
    }

    @GetMapping("delTree/{id}")
    public ResponseResult delTree(@PathVariable("id") Long id) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            //删除的逻辑，两条sql
             tbMenuService.delTree(id);
             result.setMessage("删除成功");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("删除失败:" + e.toString());
        }
        return result;
    }

    @GetMapping("geTreeByUserId/{userId}")
    public ResponseResult geTreeByUserId(@PathVariable("userId") Long userId) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            List<TreeMenuVo> list = tbMenuService.geTreeByUserId(userId);
            result.setResult(list);
            result.setMessage("查询成功");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败:" + e.toString());
        }
        return result;
    }

}
