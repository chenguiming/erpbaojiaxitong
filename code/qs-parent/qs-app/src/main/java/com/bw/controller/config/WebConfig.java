package com.bw.controller.config;


import com.bw.controller.config.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor).excludePathPatterns("/user/login")
                .excludePathPatterns("/user/download/*")
                .excludePathPatterns("/information/pi-customer/exportExcel")
                .excludePathPatterns("/information/pi-linkman/exportExcel")
                .excludePathPatterns("/process/exportExcel")
                .excludePathPatterns("/material/importExcel")
                .excludePathPatterns("/material/exportExcel")
                .excludePathPatterns("/order/pi-order-pic/importPic")
                .excludePathPatterns("/order/pi-order-pic/download/*/*")
                .excludePathPatterns("/order/exportExcel/*")
                .excludePathPatterns("/information/customerLogin/sendCode/*")
                .excludePathPatterns("/information/customerLogin/login");


}




}