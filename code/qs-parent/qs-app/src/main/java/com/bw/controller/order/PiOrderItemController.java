package com.bw.controller.order;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.fastjson.JSON;
import com.bw.controller.information.PiMaterialController;
import com.bw.entity.order.PiOrderItem;
import com.bw.resp.ResponseResult;
import com.bw.service.order.IPiOrderItemService;
import com.bw.service.order.IPiOrderService;
import com.bw.utils.excel.ExcelUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@RestController
@RequestMapping("/order/pi-order-item")
public class PiOrderItemController {

    @Autowired
    private IPiOrderService iPiOrderService;
    @Autowired
    private IPiOrderItemService iPiOrderItemService;
    private final Logger logger= LoggerFactory.getLogger(PiMaterialController.class);//日志
    @PostMapping("/importExcel")
    public ResponseResult importExcel(MultipartFile file){//导入 参数1：文件流
        logger.info("导入的xls");
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            String ext = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."), file.getOriginalFilename().length());//获取后缀
            List<Map> mapList = ExcelUtil.readExcel(ext, file.getInputStream());//根据工具类读取到Excel所有内容
            logger.info("读取到Excel所有内容");
            String jsonString = JSON.toJSONString(mapList);//转为String类型
            List<PiOrderItem> list = JSON.parseArray(jsonString, PiOrderItem.class);//转为JSON格式
            logger.info("全部内容转为JSON格式");
            for (PiOrderItem piOrderItem:list) {
                if(piOrderItem!=null){
                    result.setResult(list);
                }
            }
        }catch (Exception e){
            logger.error("导入失败"+e.toString());
            result.setMessage("导入失败"+e.toString());
            result.setSuccess(false);
        }
        return result;
    }

}
