package com.bw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling  //开启定时器
public class QsApplication {
    public static void main(String[] args) {
        SpringApplication.run(QsApplication.class);
    }
}
