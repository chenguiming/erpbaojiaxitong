package com.bw.controller.order;




import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.entity.order.PiOrderPic;
import com.bw.resp.ResponseResult;
import com.bw.service.order.IPiOrderPicService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@RestController
@RequestMapping("/order/pi-order-pic")
public class PiOrderPicController {

    @Autowired
    private IPiOrderPicService iPiOrderPicService;
    @Value("${filePath}")
    private String filePath;
    /**
     * 用户头像上传接口
     */
    @PostMapping("/importPic/{code}")
    public ResponseResult upload(MultipartFile[] file,@PathVariable  String code) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            //循环前端获取的图片
            for (MultipartFile file1:file) {
                //1，获取文件后缀
                String ext=file1.getOriginalFilename().substring(file1.getOriginalFilename().lastIndexOf("."),file1.getOriginalFilename().length());
                //2，获取路径
                String dirPath=filePath;
                String path= UUID.randomUUID()+ext;
                //3，将文件复制到对应服务器的路径下
                File mFile=new File(dirPath+path);
                file1.transferTo(mFile);
                //4，将文件的存储路径返回给前端
                System.out.println(code);

                PiOrderPic piOrderPic = new PiOrderPic();
                String th=file1.getOriginalFilename().substring(0,file1.getOriginalFilename().lastIndexOf("."));
                piOrderPic.setCode(th);//存入图纸图号
                piOrderPic.setFilePath(dirPath+path);//地址
                piOrderPic.setOrderCode(code);//订单编号
                piOrderPic.setFileSize(String.valueOf(file1.getSize()));//图纸大小
                piOrderPic.setName(path);//图纸全称
                iPiOrderPicService.saveOrUpdate(piOrderPic);
            }
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("上传失败:" + e.toString());
            System.out.println("上传失败"+e.toString());
        }
        return result;
    }
    @GetMapping("/download/{path}/{code}")
    public void download(@PathVariable("path") String path,@PathVariable String code, HttpServletResponse response) {
        if(null==path){
            return;
        }
        try {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("code",path.trim());
            wrapper.eq("orderCode",code.trim());
            PiOrderPic pic = iPiOrderPicService.getOne(wrapper);
            if(null==pic){
                ServletOutputStream outputStream = response.getOutputStream();
                response.setHeader("Content-type","text/html;charset=UTF-8");
                String error="抱歉，当前零件没有匹配图纸";
                outputStream.write(error.getBytes("utf-8"));
                return;
            }
            String ppath=pic.getFilePath();
            //读取图片文件流
            InputStream inputStream=new FileInputStream(ppath);
            byte[] b=new byte[1024];//定义每次读取输入流当中的多少字节
            int len;//记录每次读取字节流的位置

            while ((len=inputStream.read(b))>0){            //将文件流写到响应输出流当中
                response.getOutputStream().write(b,0,len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
