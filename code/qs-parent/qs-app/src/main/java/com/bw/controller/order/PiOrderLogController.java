package com.bw.controller.order;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@RestController
@RequestMapping("/order/pi-order-log")
public class PiOrderLogController {

}
