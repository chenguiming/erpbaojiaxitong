package com.bw.controller.config.interceptor;

import com.bw.utils.auth.JwtUtils;
import com.bw.utils.auth.RsaUtils;
import com.bw.utils.auth.UserInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.PublicKey;

@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        //验证token是否合法
        String token = request.getHeader("token");//获取token
        if(StringUtils.isEmpty(token)){ //token为null
                return false;
        }
        try {
            PublicKey publicKey = RsaUtils.getPublicKey("D:\\jwt\\key\\rsa.pub");
            UserInfo userInfo =   JwtUtils.getInfoFromToken(token,publicKey);
            if(userInfo!=null){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }
}
