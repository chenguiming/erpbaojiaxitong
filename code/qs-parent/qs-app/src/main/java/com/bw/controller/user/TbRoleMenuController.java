package com.bw.controller.user;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.bw.entity.user.TbRoleMenu;
import com.bw.resp.ResponseResult;
import com.bw.service.user.ITbRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@RestController
@RequestMapping("roleMenu")
public class TbRoleMenuController {

    @Autowired
    private ITbRoleMenuService roleMenuService;

    @PostMapping("save")
    public ResponseResult save(Long roleId, Long[] menuIds) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            roleMenuService.saveRoleMenu(roleId,menuIds);
            result.setMessage("授权成功");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("授权失败:" + e.toString());
        }
        return result;
    }

    @GetMapping("getByRoleId/{roleId}") //根据用户id获取角色id接口
    public ResponseResult getByRoleId(@PathVariable("roleId") Long roleId) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper queryWrapper=new QueryWrapper();
            queryWrapper.eq("ROLE_ID",roleId);
            List<TbRoleMenu> tbRoleMenus=roleMenuService.list(queryWrapper);
            List<Long> menuIds=new ArrayList<>();
            for (TbRoleMenu tbRoleMenu:tbRoleMenus) {
                menuIds.add(tbRoleMenu.getMenuId());
            }
            result.setResult(menuIds);
            result.setMessage("查询成功");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败:" + e.toString());
        }
        return result;
    }
}
