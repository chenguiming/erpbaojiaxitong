package com.bw.controller.user;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.bw.entity.user.TbUserRole;
import com.bw.resp.ResponseResult;
import com.bw.service.user.ITbUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@RestController
@RequestMapping("userRole")
public class TbUserRoleController {


    @Autowired
    private ITbUserRoleService userRoleService;


    @PostMapping("save")
    public ResponseResult save(Long userId, Long[] roleIds) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            userRoleService.saveUserRole(userId,roleIds);
            result.setMessage("分配成功");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("分配失败:" + e.toString());
        }
        return result;
    }

    @GetMapping("getByUserId/{userId}") //根据用户id获取角色id接口
    public ResponseResult getByUserId(@PathVariable("userId") Long userId) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper queryWrapper=new QueryWrapper();
            queryWrapper.eq("USER_ID",userId);
            List<TbUserRole>  tbUserRoles=userRoleService.list(queryWrapper);
            List<Long> roleIds=new ArrayList<>();
            for (TbUserRole tbUserRole:tbUserRoles) {
                roleIds.add(tbUserRole.getRoleId());
            }
            result.setResult(roleIds);
            result.setMessage("查询成功");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询列表失败:" + e.toString());
        }
        return result;
    }

}
