package com.bw.controller.information;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.entity.Invoice.PiInvoice;
import com.bw.entity.information.PiLinkman;
import com.bw.entity.order.PiOrder;
import com.bw.req.CusLoginMes;
import com.bw.resp.ResponseResult;
import com.bw.service.Invoice.IPiInvoiceService;
import com.bw.service.information.IPiCustomerService;
import com.bw.service.order.IPiOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;


@RestController
@RequestMapping("/information/customerLogin")
public class PiCustomerLogin {

    public final Logger logger = LoggerFactory.getLogger(PiCustomerLogin.class);
    @Autowired
    private IPiCustomerService iPiCustomerService;
    @Autowired
    private IPiOrderService iPiOrderService;
    @Autowired
    private IPiInvoiceService iPiInvoiceService;

    @GetMapping("/sendCode/{tel}")
    public ResponseResult detailById(@PathVariable String tel) {//查看明细 参数1：定义一个接前端传值的对象
        logger.info("发送验证码");
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            result = iPiCustomerService.sendCode(tel);//调用发送接口
        } catch (Exception e) {
            logger.error("发送验证码错误" + e.toString());
            result.setMessage("查看失败" + e.toString());
            result.setSuccess(false);
        }
        logger.info("发送验证码结束");
        return result;
    }

    /*
     * //登录  参数：手机号，验证码对象
     * */
    @PostMapping("/login")
    public ResponseResult login(@RequestBody CusLoginMes cusLoginMes) {
        logger.info("登录开始");
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            result = iPiCustomerService.loginByTelAndCode(cusLoginMes);
        } catch (Exception e) {
            logger.error("登录失败" + e.toString());
            result.setMessage("登录失败" + e.toString());
            result.setSuccess(false);
        }
        logger.info("登录结束");
        return result;
    }

    /*
     * 查询订单开始
     * 客户id   订单状态
     * 客户下的id
     * */
    @GetMapping("/AllInOrder/{id}/{state}")
    public ResponseResult AllInOrder(@PathVariable long id, @PathVariable int state) {
        logger.info("查询订单开始");
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("customerId", id);
            wrapper.eq("state", state);
            wrapper.orderByDesc("createDate");
            List list = iPiOrderService.list(wrapper);
            result.setMessage("查询成功");
            result.setResult(list);
        } catch (Exception e) {
            logger.error("登录失败" + e.toString());
            result.setMessage("登录失败" + e.toString());
            result.setSuccess(false);
        }
        logger.info("查询订单结束");
        return result;
    }

    /*
     *
     * */
    @GetMapping("/payList/{id}")
    public ResponseResult payList(@PathVariable long id) {
        logger.info("申请发票时查询金额开始");
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("customerId",id);
            wrapper.eq("state",4);
            List<PiOrder> list = iPiOrderService.list(wrapper);
            BigDecimal money = new BigDecimal("0");
            for (PiOrder piOrder:list) {
                money=money.add(piOrder.getPriceIt());
            }
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("customerId",id);
            queryWrapper.ne("state",2);
            List<PiInvoice> invoiceList = iPiInvoiceService.list(queryWrapper);
            BigDecimal money2 = new BigDecimal("0");
            for (PiInvoice piInvoice:invoiceList) {
                money2=money2.add(piInvoice.getAmount());
            }
            BigDecimal zje=money.subtract(money2);
            result.setResult(zje);
            logger.info("剩余可开发票金额"+zje);
        } catch (Exception e) {
            logger.error("申请发票时查询金额失败" + e.toString());
            result.setMessage("申请发票时查询金额失败" + e.toString());
            result.setSuccess(false);
        }
        logger.info("申请发票时查询金额结束");
        return result;
    }
}
