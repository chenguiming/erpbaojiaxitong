package com.bw.controller.craft;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@RestController
@RequestMapping("/order/pi-order-item-material")
public class PiOrderItemMaterialController {

}
