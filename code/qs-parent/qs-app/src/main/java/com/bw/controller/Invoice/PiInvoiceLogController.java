package com.bw.controller.Invoice;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.entity.Invoice.PiInvoice;
import com.bw.resp.ResponseResult;
import com.bw.service.Invoice.IPiInvoiceLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
@RestController
@RequestMapping("/Invoice/pi-invoice-log")
public class PiInvoiceLogController {
    public final Logger logger= LoggerFactory.getLogger(PiInvoiceController.class);
    @Autowired
    private IPiInvoiceLogService iPiInvoiceLogService;
    @GetMapping("/getInvoiceLog/{id}")
    public ResponseResult getInvoiceLog(@PathVariable("id") Integer id) {
        logger.info("查找发票记录开始");
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("invoiceId",id);
            wrapper.orderByDesc("updatetime");
            List list = iPiInvoiceLogService.list(wrapper);
            result.setResult(list);
            result.setMessage("查找发票记录");
        } catch (Exception e) {
            logger.info("查找发票记录失败,{}",e.toString());
            result.setSuccess(false);
            result.setMessage("查找发票记录失败:" + e.toString());
        }
        logger.info("查找发票记录结束");
        return result;
    }
}
