package com.bw.controller.craft;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.entity.craft.PiOrderItemMaterial;
import com.bw.entity.craft.PiOrderItemProcess;
import com.bw.entity.information.PiMaterial;
import com.bw.entity.information.PiProcess;
import com.bw.entity.order.PiOrderItem;
import com.bw.req.CraftAndItem;
import com.bw.req.OrderAndItem;
import com.bw.resp.ResponseResult;
import com.bw.service.craft.IPiOrderItemMaterialService;
import com.bw.service.craft.IPiOrderItemProcessService;
import com.bw.service.information.IPiMaterialService;
import com.bw.service.information.IPiProcessService;
import com.bw.service.order.IPiOrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/order/MakeCraft")
public class MakeCraft {
    @Autowired
    private IPiOrderItemService iPiOrderItemService;
    @Autowired
    private IPiMaterialService iPiMaterialService;
    @Autowired
    private IPiProcessService iPiProcessService;
    @Autowired
    private IPiOrderItemMaterialService iPiOrderItemMaterialService;
    @Autowired
    private IPiOrderItemProcessService iPiOrderItemProcessService;

    @GetMapping("/cxItem/{id}")
    public ResponseResult cxItem(@PathVariable int id) {
        /*
         * 根据id查询订单零件表的详细信息
         * */
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            PiOrderItem piOrderItem = iPiOrderItemService.getById(id);//根据id查询订单零件表的详细信息
            if (piOrderItem.getPrice() != null) {
                CraftAndItem craftAndItem = new CraftAndItem();
                QueryWrapper wrapper = new QueryWrapper();
                wrapper.eq("orderItemId", piOrderItem.getId());
                List piOrderItemMaterialList = iPiOrderItemMaterialService.list(wrapper);
                craftAndItem.setPiItem(piOrderItem);//回显零件信息
                craftAndItem.setPiOrderItemMaterial(piOrderItemMaterialList);//回显零件制作列表
                wrapper.eq("orderItemId", piOrderItem.getId());
                List piOrderItemProcessList = iPiOrderItemProcessService.list(wrapper);
                craftAndItem.setPiOrderItemProcess(piOrderItemProcessList);//回显工序列表
                result.setMessage("已经制作，现在您可以发起修改");
                result.setResult(craftAndItem);
            } else {
                result.setMessage("操作成功");
                result.setResult(piOrderItem);
            }
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("操作失败" + e.toString());
        }
        return result;
    }

    @PostMapping("/pimaterialList")
    public ResponseResult pimaterialList(@RequestBody PiMaterial piMaterial) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            List<PiMaterial> list = iPiMaterialService.list();
            result.setResult(list);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败" + e.toString());
        }
        return result;
    }

    @PostMapping("/processList")
    public ResponseResult processList(@RequestBody PiProcess PiProcess) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            List<PiProcess> list = iPiProcessService.list();
            result.setResult(list);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("查询失败" + e.toString());
        }
        return result;
    }

    @PostMapping("/add")
    public ResponseResult add(@RequestBody CraftAndItem craftAndItem) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {

            PiOrderItem piItem = craftAndItem.getPiItem();
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("orderItemId", piItem.getId());
            iPiOrderItemProcessService.remove(wrapper);//添加前先清空订单零件材料表中的数据
            iPiOrderItemMaterialService.remove(wrapper);//删除订单零件工序表中的数据
            result=iPiOrderItemMaterialService.saveByCraftAndItem(craftAndItem);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("添加失败" + e.toString());
        }
        return result;
    }
}
