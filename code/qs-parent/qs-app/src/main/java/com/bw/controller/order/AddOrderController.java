package com.bw.controller.order;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.controller.information.PiMaterialController;
import com.bw.entity.information.PiCustomer;
import com.bw.entity.order.PiOrder;
import com.bw.entity.order.PiOrderItem;
import com.bw.req.OrderAndItem;
import com.bw.resp.ResponseResult;
import com.bw.service.order.IPiOrderItemService;
import com.bw.service.order.IPiOrderService;
import com.bw.utils.excel.ExcelUtil;
import com.bw.utils.idWorkers.IdWorker;
import net.sf.excelutils.ExcelException;
import net.sf.excelutils.ExcelUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@RequestMapping("/order/addOrder")
@RestController
public class AddOrderController {
    @Autowired
    private IPiOrderService iPiOrderService;
    @Autowired
    private IPiOrderItemService iPiOrderItemService;
    private final Logger logger= LoggerFactory.getLogger(PiMaterialController.class);//日志
    @PostMapping("/resultCode")
    public ResponseResult resultCode(@RequestBody PiOrder piOrder){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            IdWorker idWorker = new IdWorker();
            long resultCode = idWorker.nextId();
            result.setResult(resultCode);
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("生成编号失败"+e.toString());
        }
        return result;
    }
    @PostMapping("/add")
    public ResponseResult add(@RequestBody OrderAndItem orderAndItem){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            iPiOrderService.saveOrderAndItem(orderAndItem);
            result.setMessage("添加成功");
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("添加失败"+e.toString());
        }
        return result;
    }
    @GetMapping("/delitalOrder/{id}")
    public ResponseResult delitalOrder(@PathVariable int id){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            PiOrder piOrder = iPiOrderService.getById(id);
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("orderId",id);
            List<PiOrderItem> list = iPiOrderItemService.list(wrapper);
            OrderAndItem orderAndItem = new OrderAndItem();
            orderAndItem.setPiOrder(piOrder);
            orderAndItem.setPiOrderItems(list);
            result.setResult(orderAndItem);
            result.setMessage("操作成功");
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("操作失败"+e.toString());
        }
        return result;
    }
}
