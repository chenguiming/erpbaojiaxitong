package com.bw.utils.aliPay;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016102600764825";

	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC1oAcP0LlSb89ghXMVL70+Pyyqwr9D8HB5mKrCQqk7TP72ObuEXcrCXHuOiVhuOwY7H+JkE6bryHR4I4SzrZhCzpBuNj3Aljlgu30PA4mZ9bPxUKVa91epCrruMLrZeX1CF5P2tad/1XqX85PdIsHAew9iTWXdZ+Mwa/Y0iMFgmtl5UpcBE0BS7TjkD0BetRhy6Z53Kt/tjyD8opMfRufMKsH/kxcmQiCNeMpDHEGX91Sze4tgjj5b+UUBZkSwpqtKL3vBto57C7P1PTemYlaFAo5t7c3gYsfZE3yIhGVzLYm2CHJA8ixU7hX7niwU35I4TpihF+vLXDwan7JTJ9E1AgMBAAECggEBAKczRsz8bV7Cp7pHoAYlCKZBKjGZTtKQijqmnKxxCbIZ062AwGu+nGYJFYXGUq5HkBjl5VgT71qoEvB2Yje227DE0JllPbwJj5DJiPtFWmttkdXZVCBGC2EHC8cy/RAvk2xAoGpCGIzKTi2MgtbUpGE56JOqhNgqRy/Qc7sxGjR/SWGwBYLOWa+a+wxHrE0uxbNaThmiwXjZAgn2W4HZOCSSudFyikP4zt+bZHqXClTin6xT6MSw8JB85pG3uoEizR+0gTqyAPSPe0SQhlnffMj7F2Ad45AnJ6jtlbwb/ABku0iSv+dbNvyXaBoahdP4sHyGC8x9P8UDR7QwNVUpqgECgYEA8rUILHHTXLCwY4YmIRJHNcaJQT7iiAlAcTR3zOXPpsfVxe/BOyDizLkxFPKp2s4TCLcvwTJoyq1aY7VEvPCDknnJCdQJDJ8y70azlVLIu78eKRG2z7O7dp1Dy/NLUUpKdxJxkTFys9ipb8uH2HWDmbkCqhUeFYtg3YiAwVje2GECgYEAv5KSUPPhsDcI777hooLhuq0w60yLX1yMfTl1YVrDZvo0O+mc0iHW8HgOuvmIDgnXT295k91qqVOw/nZiJQQxOcO28Vz9AlTlAaF4MNHRjW4xykiWxX5mWK8vz+hMGs+53UVqefWIioc9R7GybOJkFOvk/Z/yysiiMQj/lhDYmVUCgYEAlXe3WOegauzFNAVhZeyDvjJORMgdafmIni7ZuG2uu99FX2iPb4KJKdrCx0vKHt3aCvwFhM0ui+LGT7FKa7ES1hn6HPtft04zZ/f6kHMy+6xvPouSMcdWI24mWmuybpV51mqdVfVF1kwPzLFjL4LvULQHWAc5R7tWJBf0cx3v3wECgYEAl62EHYZLLcsVT0URvyrjcHtJ3TM4smNDdK52qoNDbRgzHTClEpbqaHCWMSH6wCq4h9NaLmHD3pc97utqX6aHFxZHlairRhpTS0w+LmEiClmyIFgC0BF5FUR3laR2BVBs963LVQ67cfsL4OTnwWAqH4p6PvlSMY5TJIHcY0OpSxUCgYBAUNOmDxzRhYZrby2YsbUa9WbqbrZm3gWKcWid6wcxQEaP/XOGyqQLQfBMjaSftyTXYW9y21jMIAo5lCcIy8MlhmlsHPEdENEJLKDMHWicI/zTZjw5YNggeYuVKklIlsF7MpE4MygTWvjO2oXeeudvM/8YcFZDvC0MU6iZMoXPtg==";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq9ISJbS1xhL4IgRX3q9EI2jgpjcPNL3ouCdDJHWPDjWouq0rQXp8JYGx7UHMuxcuiM4uKfxw+qGOhPZSuYYYSQ7M88fAU8BdJ3Q0JKEbHQ7p6P1XVDWMkuiPIHIV+eiVmarSgAn5cT6u/8MiEfPwdxT28r4GD91cw5uVEEjoRFWxljokgzXnyyvvYynUa5LFr7lDHQh9PNHIBn0SekFh7xhd4fsEAYgCc9NMeUVeasW+Pn8APW0kb0t6whQKHOmyhiowo7NNEd1gLPNutBlQZmud38OLt8SJ2/5dPzG65H9BkKtpKXsGLZ0vEmOgf9HBJF0/ytrCbWcCa1YAbQWcGQIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8080/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8888/#/payResult";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

