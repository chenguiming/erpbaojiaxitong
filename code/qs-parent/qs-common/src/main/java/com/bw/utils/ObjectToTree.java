package com.bw.utils;



import com.bw.resp.TreeMenuVo;

import java.util.ArrayList;
import java.util.List;

public class ObjectToTree {

    public static List<TreeMenuVo> buildTree(List<TreeMenuVo> list){
        List<TreeMenuVo> tree=new ArrayList<>();
        for (TreeMenuVo n:list) {
            if(n.getParentId()==0){
                tree.add(findChild(n,list));//添加TreeMenuVo
            }
        }
        return tree;
    }

    static TreeMenuVo findChild(TreeMenuVo node,List<TreeMenuVo> list){//查询具体的某一个节点
        for (TreeMenuVo n:list) {
            if(n.getParentId().equals(node.getId())){
                if(node.getChildren()==null){
                    node.setChildren(new ArrayList<>());
                }
                node.getChildren().add(findChild(n,list));
            }
        }
        return node;
    }
}
