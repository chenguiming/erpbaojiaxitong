package com.bw.service.information;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.information.PiLinkman;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-15
 */
public interface IPiLinkmanService extends IService<PiLinkman> {

}
