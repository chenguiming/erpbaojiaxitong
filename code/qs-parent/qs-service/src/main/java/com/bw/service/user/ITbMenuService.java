package com.bw.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.user.TbMenu;
import com.bw.resp.TreeMenuVo;


import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
public interface ITbMenuService extends IService<TbMenu> {


    public List<TreeMenuVo> getTree();

    public void delTree(Long id);

    List<TreeMenuVo> geTreeByUserId(Long userId);
}
