package com.bw.service.information.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.bw.entity.information.PiProcess;
import com.bw.mapper.information.PiProcessMapper;
import com.bw.service.information.IPiProcessService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-16
 */
@Service
public class PiProcessServiceImpl extends ServiceImpl<PiProcessMapper, PiProcess> implements IPiProcessService {

}
