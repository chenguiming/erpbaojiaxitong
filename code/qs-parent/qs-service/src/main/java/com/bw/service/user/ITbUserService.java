package com.bw.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.user.TbUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
public interface ITbUserService extends IService<TbUser> {

}
