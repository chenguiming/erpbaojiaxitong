package com.bw.service.information.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.information.PiLinkman;
import com.bw.mapper.information.PiLinkmanMapper;
import com.bw.service.information.IPiLinkmanService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-15
 */
@Service
public class PiLinkmanServiceImpl extends ServiceImpl<PiLinkmanMapper, PiLinkman> implements IPiLinkmanService {

}
