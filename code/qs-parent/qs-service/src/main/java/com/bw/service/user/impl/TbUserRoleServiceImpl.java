package com.bw.service.user.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.user.TbUserRole;
import com.bw.mapper.user.TbUserRoleMapper;
import com.bw.service.user.ITbUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@Service
public class TbUserRoleServiceImpl extends ServiceImpl<TbUserRoleMapper, TbUserRole> implements ITbUserRoleService {


    @Autowired
    private  TbUserRoleMapper userRoleMapper;

    @Override
    @Transactional
    public void saveUserRole(Long userId, Long[] roleIds) {
        //先删除当前用户的角色，然后在添加新的用户角色
        //遍历roleIds
        //保存到tb_user_role关系表

        QueryWrapper queryWrapp=new QueryWrapper();
        queryWrapp.eq("USER_ID",userId);
        userRoleMapper.delete(queryWrapp);        //删除

        List<TbUserRole> tbUserRoles=new ArrayList<>();//用户角色关系集合
        for (Long roleId:roleIds) {
            TbUserRole tbUserRole=new TbUserRole();
            tbUserRole.setUserId(userId);
            tbUserRole.setRoleId(roleId);
            tbUserRoles.add(tbUserRole);
        }
        this.saveBatch(tbUserRoles);//批量添加
    }
}
