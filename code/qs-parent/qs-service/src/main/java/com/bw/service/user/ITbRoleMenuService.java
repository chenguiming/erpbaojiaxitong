package com.bw.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.user.TbRoleMenu;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
public interface ITbRoleMenuService extends IService<TbRoleMenu> {

    public void saveRoleMenu(Long roleId, Long[] menuIds);

}
