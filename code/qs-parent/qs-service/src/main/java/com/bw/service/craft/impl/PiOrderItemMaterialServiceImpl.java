package com.bw.service.craft.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.craft.PiOrderItemMaterial;
import com.bw.entity.craft.PiOrderItemProcess;
import com.bw.entity.information.PiProcess;
import com.bw.entity.order.PiOrderItem;
import com.bw.mapper.craft.PiOrderItemMaterialMapper;
import com.bw.req.CraftAndItem;
import com.bw.resp.ResponseResult;
import com.bw.service.craft.IPiOrderItemMaterialService;
import com.bw.service.craft.IPiOrderItemProcessService;
import com.bw.service.information.IPiMaterialService;
import com.bw.service.information.IPiProcessService;
import com.bw.service.order.IPiOrderItemService;
import com.bw.service.order.IPiOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Service
public class PiOrderItemMaterialServiceImpl extends ServiceImpl<PiOrderItemMaterialMapper, PiOrderItemMaterial> implements IPiOrderItemMaterialService {
    @Autowired
    private IPiOrderItemService iPiOrderItemService;
    @Autowired
    private IPiMaterialService iPiMaterialService;
    @Autowired
    private IPiProcessService iPiProcessService;
    @Autowired
    private IPiOrderItemMaterialService iPiOrderItemMaterialService;
    @Autowired
    private IPiOrderItemProcessService iPiOrderItemProcessService;
    @Autowired
    private IPiOrderService iPiOrderService;


    @Transactional
    @Override
    public ResponseResult saveByCraftAndItem(CraftAndItem craftAndItem) {
        ResponseResult result = ResponseResult.SUCCESS();
        BigDecimal price = new BigDecimal(0);
        BigDecimal zmj=new BigDecimal(0);
        BigDecimal zzl=new BigDecimal(0);
        BigDecimal gxprice=new BigDecimal(0);
        List<PiOrderItemMaterial> piOrderItemMaterials = craftAndItem.getPiOrderItemMaterial();
        for (PiOrderItemMaterial piOrderItemMaterial:piOrderItemMaterials) {
            if(piOrderItemMaterial.getAfArea()==null || piOrderItemMaterial.getAfWeight()==null || piOrderItemMaterial.getMaterCost()==null || piOrderItemMaterial.getAfWeight()<=0 || piOrderItemMaterial.getAfArea()<=0){
                result.setSuccess(false);
                result.setMessage("输入的材料信息不能为空或负数");
                return result;
            }
            BigDecimal materCost = piOrderItemMaterial.getMaterCost();
            price=price.add(materCost);
            piOrderItemMaterial.setOrderItemId(craftAndItem.getPiItem().getId());
            zmj=zmj.add(new BigDecimal(piOrderItemMaterial.getAfArea()));//总面积
            zzl=zzl.add(new BigDecimal(piOrderItemMaterial.getAfWeight()));//总重量
            iPiOrderItemMaterialService.saveOrUpdate(piOrderItemMaterial);
        }

        //工时费计算
        List<PiOrderItemProcess> piOrderItemProcess = craftAndItem.getPiOrderItemProcess();
        for (PiOrderItemProcess piOrderItemProcesses:piOrderItemProcess) {
            if(piOrderItemProcesses.getPreWorkHour()==null || piOrderItemProcesses.getSgWorkHour()==null){
                result.setSuccess(false);
                result.setMessage("输入的工时不合法");
                return result;
            }
            piOrderItemProcesses.setOrderItemId(craftAndItem.getPiItem().getId());//订单零件id
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("id",piOrderItemProcesses.getProcessId());
            PiProcess piProcess = iPiProcessService.getOne(wrapper);
            //准备工时费
            BigDecimal preCost=(piProcess.getPrePrice().multiply(new BigDecimal(piOrderItemProcesses.getPreWorkHour()))).divide(new BigDecimal((craftAndItem.getPiItem().getNum())));
            piOrderItemProcesses.setPreCost(preCost);
            //工序费用
            BigDecimal zgx=new BigDecimal(0);
            if(piProcess.getCtype()==1){
                //时间
                zgx=(piProcess.getPrice().multiply(new BigDecimal(piOrderItemProcesses.getSgWorkHour())));
            }else if(piProcess.getCtype()==2){
                //面积
                zgx=(piProcess.getPrice().multiply(zmj));
            }else if(piProcess.getCtype()==3){
                //重量
                zgx=(piProcess.getPrice().multiply(zzl));
            }
            //加工费
            piOrderItemProcesses.setProCost(preCost.add(zgx));//加工费添加
            gxprice=gxprice.add(piOrderItemProcesses.getProCost());
            iPiOrderItemProcessService.saveOrUpdate(piOrderItemProcesses);
        }
        //获取当前的订单零件表
        PiOrderItem piOrderItem = iPiOrderItemService.getById(craftAndItem.getPiItem().getId());
        piOrderItem.setPrice(price.add(gxprice));
        iPiOrderItemService.saveOrUpdate(piOrderItem);
        result.setMessage("添加成功");
        return result;
    }
}
