package com.bw.service.order.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.order.PiOrderPic;
import com.bw.mapper.order.PiOrderPicMapper;
import com.bw.service.order.IPiOrderPicService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Service
public class PiOrderPicServiceImpl extends ServiceImpl<PiOrderPicMapper, PiOrderPic> implements IPiOrderPicService {

}
