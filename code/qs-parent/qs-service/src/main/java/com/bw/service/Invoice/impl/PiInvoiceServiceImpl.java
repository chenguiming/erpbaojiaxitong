package com.bw.service.Invoice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.Invoice.PiInvoice;
import com.bw.mapper.Invoice.PiInvoiceMapper;
import com.bw.service.Invoice.IPiInvoiceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
@Service
public class PiInvoiceServiceImpl extends ServiceImpl<PiInvoiceMapper, PiInvoice> implements IPiInvoiceService {

}
