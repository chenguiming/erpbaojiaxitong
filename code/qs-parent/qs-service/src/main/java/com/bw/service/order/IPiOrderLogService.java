package com.bw.service.order;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.order.PiOrderLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
public interface IPiOrderLogService extends IService<PiOrderLog> {

}
