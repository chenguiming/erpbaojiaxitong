package com.bw.service.information;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.information.PiProcess;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-16
 */
public interface IPiProcessService extends IService<PiProcess> {

}
