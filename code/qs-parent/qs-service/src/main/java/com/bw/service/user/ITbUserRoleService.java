package com.bw.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.user.TbUserRole;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
public interface ITbUserRoleService extends IService<TbUserRole> {

    public void saveUserRole(Long userId, Long[] roleIds);

}
