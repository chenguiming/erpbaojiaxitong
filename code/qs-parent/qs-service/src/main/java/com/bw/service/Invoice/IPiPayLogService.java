package com.bw.service.Invoice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.Invoice.PiPayLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
public interface IPiPayLogService extends IService<PiPayLog> {

}
