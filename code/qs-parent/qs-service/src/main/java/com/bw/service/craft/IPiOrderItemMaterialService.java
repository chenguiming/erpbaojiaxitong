package com.bw.service.craft;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.craft.PiOrderItemMaterial;
import com.bw.req.CraftAndItem;
import com.bw.resp.ResponseResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
public interface IPiOrderItemMaterialService extends IService<PiOrderItemMaterial> {

    ResponseResult saveByCraftAndItem(CraftAndItem craftAndItem);
}
