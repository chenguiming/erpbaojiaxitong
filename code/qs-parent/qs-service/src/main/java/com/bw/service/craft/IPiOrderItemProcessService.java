package com.bw.service.craft;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.craft.PiOrderItemProcess;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
public interface IPiOrderItemProcessService extends IService<PiOrderItemProcess> {

}
