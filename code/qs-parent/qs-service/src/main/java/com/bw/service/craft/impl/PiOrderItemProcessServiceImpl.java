package com.bw.service.craft.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.craft.PiOrderItemProcess;
import com.bw.mapper.craft.PiOrderItemProcessMapper;
import com.bw.service.craft.IPiOrderItemProcessService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Service
public class PiOrderItemProcessServiceImpl extends ServiceImpl<PiOrderItemProcessMapper, PiOrderItemProcess> implements IPiOrderItemProcessService {

}
