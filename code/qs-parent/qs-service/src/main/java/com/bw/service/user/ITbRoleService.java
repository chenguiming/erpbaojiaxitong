package com.bw.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.user.TbRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
public interface ITbRoleService extends IService<TbRole> {

}
