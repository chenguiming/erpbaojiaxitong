package com.bw.service.information;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.information.PiMaterial;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-16
 */
public interface IPiMaterialService extends IService<PiMaterial> {

}
