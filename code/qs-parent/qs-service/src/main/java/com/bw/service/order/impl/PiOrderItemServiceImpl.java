package com.bw.service.order.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.order.PiOrderItem;
import com.bw.mapper.order.PiOrderItemMapper;
import com.bw.service.order.IPiOrderItemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Service
public class PiOrderItemServiceImpl extends ServiceImpl<PiOrderItemMapper, PiOrderItem> implements IPiOrderItemService {

}
