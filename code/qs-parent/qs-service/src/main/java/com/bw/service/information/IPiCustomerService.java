package com.bw.service.information;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.information.PiCustomer;
import com.bw.req.CusLoginMes;
import com.bw.resp.ResponseResult;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-15
 */
public interface IPiCustomerService extends IService<PiCustomer> {


    ResponseResult sendCode(String tel);

    ResponseResult loginByTelAndCode(CusLoginMes cusLoginMes);

    List<Map> getTechnologyNum();

    List<Map> getCustomerNum();
}
