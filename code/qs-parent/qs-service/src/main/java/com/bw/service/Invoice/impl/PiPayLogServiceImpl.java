package com.bw.service.Invoice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.Invoice.PiPayLog;
import com.bw.mapper.Invoice.PiPayLogMapper;
import com.bw.service.Invoice.IPiPayLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
@Service
public class PiPayLogServiceImpl extends ServiceImpl<PiPayLogMapper, PiPayLog> implements IPiPayLogService {

}
