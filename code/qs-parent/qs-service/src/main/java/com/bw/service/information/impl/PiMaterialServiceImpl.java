package com.bw.service.information.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.information.PiMaterial;
import com.bw.mapper.information.PiMaterialMapper;
import com.bw.service.information.IPiMaterialService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-16
 */
@Service
public class PiMaterialServiceImpl extends ServiceImpl<PiMaterialMapper, PiMaterial> implements IPiMaterialService {

}
