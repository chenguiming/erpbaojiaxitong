package com.bw.service.user.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.bw.entity.user.TbMenu;
import com.bw.mapper.user.TbMenuMapper;
import com.bw.resp.TreeMenuVo;
import com.bw.service.user.ITbMenuService;
import com.bw.utils.ObjectToTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@Service
public class TbMenuServiceImpl extends ServiceImpl<TbMenuMapper, TbMenu> implements ITbMenuService {

    @Autowired
    private TbMenuMapper tbMenuMapper;

    @Override
    public List<TreeMenuVo> getTree() { //获取菜单树的集合数据
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.orderByAsc("ORDER_NUM");
        List<TbMenu> tbMenuList = tbMenuMapper.selectList(queryWrapper);//查询全部的菜单数据

        List<TreeMenuVo> treeMenuVos=new ArrayList<>();

        for (TbMenu tbMenu: tbMenuList) {
            TreeMenuVo treeMenuVo=new TreeMenuVo();
            treeMenuVo.setId(tbMenu.getId());
            treeMenuVo.setIcon(tbMenu.getIcon());
            treeMenuVo.setMenuName(tbMenu.getMenuName());
            treeMenuVo.setMenuType(tbMenu.getMenuType());
            treeMenuVo.setUrl(tbMenu.getUrl());
            treeMenuVo.setOrderNum(tbMenu.getOrderNum());
            treeMenuVo.setParentId(tbMenu.getParentId());
            treeMenuVos.add(treeMenuVo);
        }
        return ObjectToTree.buildTree(treeMenuVos);
    }

    //把一个List转换为树[给一个集合，转换为树，集合中必须有id和parentid ]
    public List<TreeMenuVo> buildTree(List<TreeMenuVo> list){
        List<TreeMenuVo> tree=new ArrayList<>();
        for (TreeMenuVo n:list) {
            if(n.getParentId()==0){
                tree.add(findChild(n,list));//添加TreeMenuVo
            }
        }
        return tree;
    }

    TreeMenuVo findChild(TreeMenuVo node,List<TreeMenuVo> list){//查询具体的某一个节点
        for (TreeMenuVo n:list) {
            if(n.getParentId().equals(node.getId())){
                    if(node.getChildren()==null){
                        node.setChildren(new ArrayList<>());
                    }
                node.getChildren().add(findChild(n,list));
            }
        }
        return node;
    }


    @Override
    @Transactional  //开启事务
    public void delTree(Long id) {
            //先删除当前节点下的所有子节点
            QueryWrapper queryWrapper=new QueryWrapper();
            queryWrapper.eq("PARENT_ID",id);
            tbMenuMapper.delete(queryWrapper);
            //在删除当前节点
            tbMenuMapper.deleteById(id);
    }

    @Override
    public List<TreeMenuVo> geTreeByUserId(Long userId) {

        List<TbMenu> tbMenuList = tbMenuMapper.getTreeByUserId(userId);//根据当前的用户id查询菜单数据

        List<TreeMenuVo> treeMenuVos=new ArrayList<>();
        for (TbMenu tbMenu: tbMenuList) {
            TreeMenuVo treeMenuVo=new TreeMenuVo();
            treeMenuVo.setId(tbMenu.getId());
            treeMenuVo.setIcon(tbMenu.getIcon());
            treeMenuVo.setMenuName(tbMenu.getMenuName());
            treeMenuVo.setMenuType(tbMenu.getMenuType());
            treeMenuVo.setUrl(tbMenu.getUrl());
            treeMenuVo.setOrderNum(tbMenu.getOrderNum());
            treeMenuVo.setParentId(tbMenu.getParentId());
            treeMenuVos.add(treeMenuVo);
        }

        return ObjectToTree.buildTree(treeMenuVos);
    }

}
