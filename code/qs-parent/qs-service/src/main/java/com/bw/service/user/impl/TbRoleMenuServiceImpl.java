package com.bw.service.user.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.bw.entity.user.TbRoleMenu;
import com.bw.mapper.user.TbRoleMenuMapper;
import com.bw.service.user.ITbRoleMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@Service
public class TbRoleMenuServiceImpl extends ServiceImpl<TbRoleMenuMapper, TbRoleMenu> implements ITbRoleMenuService {

    @Override
    @Transactional
    public void saveRoleMenu(Long roleId, Long[] menuIds) {

        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("ROLE_ID",roleId);
        this.remove(queryWrapper);//删除当前角色的下的菜单ids

        List<TbRoleMenu> tbRoleMenus=new ArrayList<>();
        for (Long menuId:menuIds) {
            if(menuId!=0L){
                TbRoleMenu tbRoleMenu=new TbRoleMenu();
                tbRoleMenu.setRoleId(roleId);
                tbRoleMenu.setMenuId(menuId);
                tbRoleMenus.add(tbRoleMenu);
            }
        }
        this.saveBatch(tbRoleMenus);//批量保存
    }
}
