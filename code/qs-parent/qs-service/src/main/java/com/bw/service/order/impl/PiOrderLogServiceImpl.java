package com.bw.service.order.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.order.PiOrderLog;
import com.bw.mapper.order.PiOrderLogMapper;
import com.bw.service.order.IPiOrderLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Service
public class PiOrderLogServiceImpl extends ServiceImpl<PiOrderLogMapper, PiOrderLog> implements IPiOrderLogService {

}
