package com.bw.service.order;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.order.PiOrderPic;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
public interface IPiOrderPicService extends IService<PiOrderPic> {

}
