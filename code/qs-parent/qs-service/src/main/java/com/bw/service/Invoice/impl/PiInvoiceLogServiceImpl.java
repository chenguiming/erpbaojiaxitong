package com.bw.service.Invoice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bw.entity.Invoice.PiInvoiceLog;
import com.bw.mapper.Invoice.PiInvoiceLogMapper;
import com.bw.service.Invoice.IPiInvoiceLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
@Service
public class PiInvoiceLogServiceImpl extends ServiceImpl<PiInvoiceLogMapper, PiInvoiceLog> implements IPiInvoiceLogService {

}
