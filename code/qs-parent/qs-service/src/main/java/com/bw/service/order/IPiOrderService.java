package com.bw.service.order;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bw.entity.order.PiOrder;

import com.bw.entity.order.PiOrderItem;
import com.bw.req.OrderAndItem;
import com.bw.req.OrderExp;
import com.bw.resp.ResponseResult;
import org.springframework.core.annotation.Order;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
public interface IPiOrderService extends IService<PiOrder> {


    boolean saveOrderAndItem(OrderAndItem orderAndItem);

    OrderExp find(Long id);

    ResponseResult payAgin(PiOrder piOrder,long id);

    List<Map> getOrderPrice();

    List<Map> getxiaoshoue();
}
