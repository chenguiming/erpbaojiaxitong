package com.bw.mapper.craft;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.craft.PiOrderItemMaterial;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
public interface PiOrderItemMaterialMapper extends BaseMapper<PiOrderItemMaterial> {

}
