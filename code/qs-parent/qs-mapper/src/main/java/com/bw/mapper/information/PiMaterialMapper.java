package com.bw.mapper.information;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.information.PiMaterial;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-16
 */
public interface PiMaterialMapper extends BaseMapper<PiMaterial> {

}
