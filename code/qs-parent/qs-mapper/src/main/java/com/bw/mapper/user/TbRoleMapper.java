package com.bw.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.user.TbRole;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
public interface TbRoleMapper extends BaseMapper<TbRole> {

}
