package com.bw.mapper.information;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.information.PiLinkman;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-15
 */
public interface PiLinkmanMapper extends BaseMapper<PiLinkman> {

}
