package com.bw.mapper.Invoice;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.Invoice.PiInvoice;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
public interface PiInvoiceMapper extends BaseMapper<PiInvoice> {

}
