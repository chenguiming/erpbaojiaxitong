package com.bw.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.order.PiOrderPic;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
public interface PiOrderPicMapper extends BaseMapper<PiOrderPic> {

}
