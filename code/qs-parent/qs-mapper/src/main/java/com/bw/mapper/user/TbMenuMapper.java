package com.bw.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.user.TbMenu;


import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
public interface TbMenuMapper extends BaseMapper<TbMenu> {

    public List<TbMenu> getTreeByUserId(Long userId);
}
