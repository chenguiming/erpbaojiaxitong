package com.bw.mapper.information;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.information.PiCustomer;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-15
 */
public interface PiCustomerMapper extends BaseMapper<PiCustomer> {
    List<Map> getCustomerNum();

    List<Map> getTechnologyNum();
}
