package com.bw.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.order.PiOrder;
import org.springframework.core.annotation.Order;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
public interface PiOrderMapper extends BaseMapper<PiOrder> {

    List<Map> getxiaoshoue();

    List<Map> getOrderPrice();
}
