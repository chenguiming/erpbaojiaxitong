package com.bw.mapper.Invoice;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bw.entity.Invoice.PiInvoiceLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
public interface PiInvoiceLogMapper extends BaseMapper<PiInvoiceLog> {

}
