package com.bw.entity.Invoice;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PiPayLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ֧???????
     */
    private String outTradeNo;

    /**
     * ???????
     */
    private Date createTime;

    /**
     * ֧?????Ԫ??
     */
    private BigDecimal totalAmount;

    /**
     * ?û?ID
     */
    private long userId;

    /**
     * ???׺??
     */
    private String tradeNo;

    /**
     * ????״̬
     */
    private Integer tradeState;

    /**
     * ?????????б
     */
    private String orderList;

    /**
     * ֧?????
     */
    private Integer payType;


}
