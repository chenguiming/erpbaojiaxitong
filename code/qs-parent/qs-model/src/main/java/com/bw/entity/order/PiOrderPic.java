package com.bw.entity.order;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("pi_order_pic")
public class PiOrderPic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????????id
     */
    @TableField("itemId")
    private Long itemId;

    /**
     * ͼ?
     */
    private String code;

    /**
     * ???
     */
    private String name;

    /**
     * ·??
     */
    @TableField("filePath")
    private String filePath;

    /**
     * ?ļ??
     */
    @TableField("fileSize")
    private String fileSize;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @TableField("orderCode")
    private String orderCode;


}
