package com.bw.entity.order;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PiOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ???????
     */
    private String code;

    /**
     * ?ͻ?id
     */
    @TableField("customerId")
    private Long customerId;

    /**
     * ?ͻ????
     */
    @TableField("customerName")
    private String customerName;

    /**
     * ??ϵ??id
     */
    @TableField("linkmanId")
    private Long linkmanId;

    /**
     * ????
     */
    @TableField("tb__ID")
    private Long tbId;

    /**
     * ??ϵ??????
     */
    @TableField("linkmanName")
    private String linkmanName;

    /**
     * ??ϵ?˵绰
     */
    @TableField("linkmanTel")
    private String linkmanTel;

    /**
     * ˰?
     */
    @TableField("taxRate")
    private BigDecimal taxRate;

    /**
     * ?۸????˰??
     */
    private BigDecimal price;

    /**
     * ?۸??˰??
     */
    @TableField("priceIt")
    private BigDecimal priceIt;

    /**
     * ????ԱId
     */
    @TableField("technicianId")
    private Long technicianId;

    /**
     * ????ʱ?
     */
    @TableField("technologyDate")
    private Date technologyDate;

    /**
     * ????ʱ?
     */
    @TableField("createDate")
    private Date createDate;

    /**
     * ?????
     */
    @TableField("updateUser")
    private Long updateUser;

    /**
     * ????ʱ?
     */
    @TableField("updateDate")
    private Date updateDate;

    /**
     * ״̬??0δ?ύ??1???ύ??2?ѹ??գ?3?ѱ??ۣ?4??ת??ͬ??
     */
    private Integer state;

    /**
     * ɾ????ʶ??0δɾ????1??ɾ????
     */
    @TableField("isDelete")
    private Integer isDelete;


}
