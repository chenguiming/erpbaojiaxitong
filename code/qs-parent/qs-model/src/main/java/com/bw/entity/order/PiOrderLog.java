package com.bw.entity.order;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("pi_order_log")
public class PiOrderLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ??????????
     */
    @TableField("userName")
    private String userName;

    /**
     * ????????id
     */
    @TableField("orderId")
    private Long orderId;

    /**
     * ????
     */
    @TableField("tb__ID")
    private Long tbId;

    /**
     * ????˵??
     */
    private String remarks;


    private Date updatetime;

}
