package com.bw.req;

import com.bw.entity.craft.PiOrderItemMaterial;
import com.bw.entity.craft.PiOrderItemProcess;
import com.bw.entity.order.PiOrderItem;

import java.util.List;

public class CraftAndItem {
    private PiOrderItem PiItem;
    private List<PiOrderItemMaterial> piOrderItemMaterial;
    private List<PiOrderItemProcess> piOrderItemProcess;

    public PiOrderItem getPiItem() {
        return PiItem;
    }

    public void setPiItem(PiOrderItem piItem) {
        PiItem = piItem;
    }

    public List<PiOrderItemMaterial> getPiOrderItemMaterial() {
        return piOrderItemMaterial;
    }

    public void setPiOrderItemMaterial(List<PiOrderItemMaterial> piOrderItemMaterial) {
        this.piOrderItemMaterial = piOrderItemMaterial;
    }

    public List<PiOrderItemProcess> getPiOrderItemProcess() {
        return piOrderItemProcess;
    }

    public void setPiOrderItemProcess(List<PiOrderItemProcess> piOrderItemProcess) {
        this.piOrderItemProcess = piOrderItemProcess;
    }

    @Override
    public String toString() {
        return "CraftAndItem{" +
                "PiItem=" + PiItem +
                ", piOrderItemMaterial=" + piOrderItemMaterial +
                ", piOrderItemProcess=" + piOrderItemProcess +
                '}';
    }
}
