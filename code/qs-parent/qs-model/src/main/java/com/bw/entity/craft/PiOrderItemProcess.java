package com.bw.entity.craft;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("pi_order_item_process")
public class PiOrderItemProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ????????ID
     */
    @TableField("orderItemId")
    private Long orderItemId;

    /**
     * ???
     */
    @TableField("processId")
    private Long processId;

    /**
     * ׼????ʱ??λ???
     */
    @TableField("preWorkHour")
    private Double preWorkHour;

    /**
     * ??????ʱ??λ???
     */
    @TableField("sgWorkHour")
    private Double sgWorkHour;

    /**
     * ????˳?
     */
    private Integer sn;

    /**
     * ???????
     */
    @TableField("proCost")
    private BigDecimal proCost;

    /**
     * ׼????ʱ???
     */
    @TableField("preCost")
    private BigDecimal preCost;


}
