package com.bw.req;

import com.bw.entity.information.PiCustomer;
import com.bw.entity.order.PiOrder;
import com.bw.entity.user.TbUser;
import lombok.Data;

import java.util.List;
@Data
public class OrderExp {
    private TbUser user;
    private PiCustomer customer;
    private PiOrder order;
    private List<OrderItem> orderItemList;

}
