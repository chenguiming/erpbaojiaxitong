package com.bw.req;

import com.bw.entity.order.PiOrder;
import com.bw.entity.order.PiOrderItem;

import java.util.List;

public class OrderAndItem {
    private PiOrder piOrder;
    private List<PiOrderItem> piOrderItems;

    public PiOrder getPiOrder() {
        return piOrder;
    }

    public void setPiOrder(PiOrder piOrder) {
        this.piOrder = piOrder;
    }

    public List<PiOrderItem> getPiOrderItems() {
        return piOrderItems;
    }

    public void setPiOrderItems(List<PiOrderItem> piOrderItems) {
        this.piOrderItems = piOrderItems;
    }

    @Override
    public String toString() {
        return "OrderAndItem{" +
                "piOrder=" + piOrder +
                ", piOrderItems=" + piOrderItems +
                '}';
    }
}
