package com.bw.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TbUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID",type= IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    @TableField("USER_NAME")
    private String userName;

    /**
     * 密码
     */
    @TableField("PWD")
    private String pwd;

    /**
     * 手机号
     */
    @TableField("TEL")
    private String tel;

    /**
     * 邮箱
     */
    @TableField("EMAIL")
    private String email;

    /**
     * 姓名
     */
    @TableField("FULL_NAME")
    private String fullName;

    /**
     * 性别
     */
    @TableField("SEX")
    private Integer sex;

    /**
     * 头像
     */
    @TableField("IMAGE")
    private String image;

    /**
     * 入职日期
     */
    @TableField("HIREDATE")
    private Date hiredate;

    /**
     * 是否启用
     */
    @TableField("IS_ENABLE")
    private Integer isEnable;


}
