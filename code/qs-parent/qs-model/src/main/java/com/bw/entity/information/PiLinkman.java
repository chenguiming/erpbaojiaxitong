package com.bw.entity.information;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PiLinkman implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ????
     */
    private String name;

    /**
     * ?Ա???0??,1Ů??
     */
    private Integer sex;

    /**
     * ?绰???
     */
    private String tel;

    /**
     * ???
     */
    private String email;

    /**
     * ?ռ???ַ
     */
    @TableField("recvAddress")
    private String recvAddress;

    /**
     * ְλ
     */
    private String position;

    /**
     * ????
     */
    private String remarks;

    /**
     * ?ͻ?id
     */
    @TableField("customerId")
    private Long customerId;

    private Integer isdelete;


}
