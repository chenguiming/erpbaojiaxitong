package com.bw.entity.craft;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("pi_order_item_material")
public class PiOrderItemMaterial implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ????????id
     */
    @TableField("orderItemId")
    private Long orderItemId;

    /**
     * ԭ????id
     */
    @TableField("materialId")
    private Long materialId;

    /**
     * ??????0?壬1?
     */
    private Integer spec;

    /**
     * ???ϳ???λmm
     */
    @TableField("bfLength")
    private Double bfLength;

    /**
     * ???Ͽ��λmm
     */
    @TableField("bfWidth")
    private Double bfWidth;

    /**
     * ???ϸߵ?λmm
     */
    @TableField("bfHight")
    private Double bfHight;

    /**
     * ????ֱ????λmm
     */
    @TableField("bfDiam")
    private Double bfDiam;

    /**
     * ??Ʒ????λmm
     */
    @TableField("afLength")
    private Double afLength;

    /**
     * ??Ʒ?��λmm
     */
    @TableField("afWidth")
    private Double afWidth;

    /**
     * ??Ʒ?ߵ?λmm
     */
    @TableField("afHight")
    private Double afHight;

    /**
     * ??Ʒֱ????λmm
     */
    @TableField("afDiam")
    private Double afDiam;

    /**
     * ??Ʒ????????ͨ????Ʒ?ĳߴ????㣩
     */
    @TableField("afArea")
    private Double afArea;

    /**
     * ??Ʒ???????????????ϵ???????/?ӹ???????
     */
    @TableField("afWeight")
    private Double afWeight;

    /**
     * ?????ó?ÿ?ֲ??ϵķ??
     */
    @TableField("materCost")
    private BigDecimal materCost;

    private Integer num;
}
