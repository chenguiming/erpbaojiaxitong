package com.bw.resp;

import java.util.List;


public class TreeMenuVo {

    private Long id;

    private String menuName;

    private String url;

    private String icon;

    private Integer orderNum;

    private Long parentId;

    private Integer menuType;

    private List<TreeMenuVo> children;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }

    public List<TreeMenuVo> getChildren() {
        return children;
    }

    public void setChildren(List<TreeMenuVo> children) {
        this.children = children;
    }
}
