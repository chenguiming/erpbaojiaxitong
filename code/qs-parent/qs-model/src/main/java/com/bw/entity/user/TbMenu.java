package com.bw.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TbMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID",type= IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    @TableField("MENU_NAME")
    private String menuName;

    /**
     * 跳转路径
     */
    @TableField("URL")
    private String url;

    /**
     * 图标
     */
    @TableField("ICON")
    private String icon;

    /**
     * 排序
     */
    @TableField("ORDER_NUM")
    private Integer orderNum;

    /**
     * 父级菜单ID
     */
    @TableField("PARENT_ID")
    private Long parentId;

    /**
     * 菜单类型
     */
    @TableField("MENU_TYPE")
    private Integer menuType;
}
