package com.bw.entity.information;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PiProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ???
     */
    private String code;

    /**
     * ???
     */
    private String name;

    /**
     * ?շѷ?ʽ??0??ʱ?䣬1????????2??????????
     */
    @TableField("ctype")
    private Integer ctype;

    /**
     * ???
     */
    private BigDecimal price;

    /**
     * ׼????ʱ???
     */
    @TableField("prePrice")
    private BigDecimal prePrice;

    /**
     * ????
     */
    private String remarks;

    @TableField("isdelete")
    private Integer isdelete;


}
