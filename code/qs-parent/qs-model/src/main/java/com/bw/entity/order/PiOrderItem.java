package com.bw.entity.order;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("pi_order_item")
public class PiOrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ???
     */
    private String name;

    /**
     * ͼ?
     */
    private String code;

    /**
     * ????????
     */
    private Integer num;

    /**
     * ???
     */
    private BigDecimal price;

    /**
     * ????id
     */
    @TableField("orderId")
    private Long orderId;

    /**
     * ?????
     */
    private Integer sn;


}
