package com.bw.entity.Invoice;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PiInvoice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ???
     */
    private String code;

    /**
     * ?ͻ?id
     */
    @TableField("customerId")
    private Long customerId;

    /**
     * ??Ʊ????(0??ֵ˰??ͨ??Ʊ??1??ֵ˰ר?÷?Ʊ)
     */
    @TableField("invoiceType")
    private Integer invoiceType;

    /**
     * ??Ʊ̧ͷ
     */
    @TableField("invoiceTitle")
    private String invoiceTitle;

    /**
     * ˰?
     */
    @TableField("invoiceNum")
    private String invoiceNum;

    /**
     * ˰?
     */
    @TableField("taxRate")
    private BigDecimal taxRate;

    /**
     * ??Ʊ?
     */
    private BigDecimal amount;

    /**
     * ??Ʊ???
     */
    private String content;

    /**
     * ?ռ??
     */
    private String receiver;

    /**
     * ?ջ???ַ
     */
    @TableField("revAddress")
    private String revAddress;

    /**
     * ??ϵ?绰
     */
    private String tel;

    /**
     * ??????˾
     */
    @TableField("logisticsCym")
    private String logisticsCym;

    /**
     * ??????˾
     */
    @TableField("logisticsCode")
    private String logisticsCode;

    /**
     * ?????
     */
    @TableField("createUser")
    private Long createUser;

    /**
     * ????ʱ?
     */
    @TableField("createDate")
    private Date createDate;

    /**
     * ?????
     */
    @TableField("updateUser")
    private Long updateUser;

    /**
     * ????ʱ?
     */
    @TableField("updateDate")
    private Date updateDate;

    /**
     * ״̬??0δ?ύ??1???ύ??2?ѹ??գ?3?ѱ??ۣ?4??ת??ͬ??
     */
    private Integer state;

    /**
     * ɾ????ʶ??0δɾ????1??ɾ????
     */
    @TableField("isDelete")
    private Integer isDelete;

    @TableField("customername")
    private String customername;

    @TableField("remarks")
    private String remarks;
}
