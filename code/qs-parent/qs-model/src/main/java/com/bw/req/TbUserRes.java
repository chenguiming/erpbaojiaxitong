package com.bw.req;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-09
 */
@Data
public class TbUserRes implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String userName;

    private String pwd;

    private String tel;

    private String email;

    private String fullName;

    private Integer sex;

    private String image;

    private Date hiredate;

    private Integer isEnable;

    private Date startDate;

    private Date endDate;


}
