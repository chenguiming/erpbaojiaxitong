package com.bw.entity.information;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PiMaterial implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ???
     */
    private String code;

    /**
     * ???
     */
    private String name;

    /**
     * ??ѧԪ?ؽз?
     */
    private String chemical;

    /**
     * ?ܶȵ?λg/cm3
     */
    private Double density;

    /**
     * Ӳ?ȵ?λhrc
     */
    private Double hardness;

    /**
     * ???Ͻ??
     */
    private String remarks;

    /**
     * ???ϵ??
     */
    private BigDecimal price;

    private Integer isdelete;


}
