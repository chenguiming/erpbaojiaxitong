package com.bw.entity.information;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PiCustomer implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ???
     */
    private String code;

    /**
     * ???
     */
    private String name;

    /**
     * ??Ʊ????(0??ֵ˰??ͨ??Ʊ??1??ֵ˰ר?÷?Ʊ)
     */
    @TableField("invoiceType")
    private Integer invoiceType;

    /**
     * ??Ʊ̧ͷ
     */
    @TableField("invoiceTitle")
    private String invoiceTitle;

    /**
     * ˰?
     */
    @TableField("invoiceNum")
    private String invoiceNum;

    /**
     * ˰?
     */
    @TableField("taxRate")
    private BigDecimal taxRate;

    /**
     * ??˾??ַ
     */
    private String address;

    /**
     * ??????ҵ(0??е???죬1???ز???2???ڣ?3????)
     */
    private Integer industry;

    /**
     * ???????
     */
    @TableField("cooperDate")
    private Date cooperDate;

    /**
     * ????
     */
    private String remarks;

    private Integer isdelete;


}
