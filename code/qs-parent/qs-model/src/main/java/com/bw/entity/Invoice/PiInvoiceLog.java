package com.bw.entity.Invoice;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PiInvoiceLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ????
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * ??????id
     */
    @TableField("userId")
    private Long userId;

    /**
     * ??????????
     */
    @TableField("userName")
    private String userName;

    /**
     * ??????Ʊid
     */
    @TableField("invoiceId")
    private Long invoiceId;

    /**
     * ????˵??
     */
    private String remarks;


    private Date updatetime;


}
