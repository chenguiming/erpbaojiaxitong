package com.bw.req;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderItem {

    //序号
    private Long no;
    //图号
    private String code;
    //零件名称
    private String name;
    //材料
    private String materialname;
    //表面处理
    private String bcnames;
    //数量
    private Integer total;
    //工时
    private Double workhour;
    //材料费
    private BigDecimal materialcost;
    //加工费
    private BigDecimal workcost;
    //热处理费
    private BigDecimal heatcost;
    //表面处理费
    private BigDecimal surfacecost;
    //含税单价
    private BigDecimal productprice;
    //总价
    private BigDecimal totalprice;

}
